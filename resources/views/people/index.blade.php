@extends('adminlte::page') 

@section('CRUD Pessoa', 'Dashboard') 

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @include('people.includes.add')

        <div class="box box-solid">
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table condensed" id="people-table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>CPF</th>
                                <th>Telefone</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $people as $person )
                                <tr>
                                    <td>{{ $person->id }}</td>
                                    <td>{{ $person->name }}</td>
                                    <td>{{ $person->cpf }}</td>
                                    <td>{{ $person->phone_number }}</td>
                                    <td class="text-right" width="190px;">
                                        <div class="text-right">
                                            <button 
                                                id="edit-button-{{ $person->id }}"
                                                type="button" 
                                                class="btn btn-xs btn-warning" 
                                                onclick="editPerson({{ $person->id }})"
                                                data-loading-text='<i class="fa fa-fw fa-spinner"></i> Carregando ...'
                                            >
                                                <i class="fa fa-fw fa-edit"></i> editar
                                            </button>

                                            <button 
                                                type="button" 
                                                class="btn btn-xs btn-danger" 
                                                onclick="deletePerson({{$person->id}})"
                                            >
                                                <i class="fa fa-fw fa-trash"></i> deletar
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('people.includes.edit')
@include('people.includes.delete_form')
@stop

@section('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
@stop