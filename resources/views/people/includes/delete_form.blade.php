<form id="form-delete-person" method="POST">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>