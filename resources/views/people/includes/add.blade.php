<div class="box box-solid">
    <div class="box-body">
        <form action="{{ route('pessoas.store') }}" method="POST">
        {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Nome</label>
                        <input class="form-control" name="name" placeholder="Digite um nome" requied>
                    </div>
                </div>
            

                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="cpf">CPF</label>
                        <input class="form-control" name="cpf" placeholder="000.000.000-00" required>
                    </div>
                </div>
                
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="phone_number">Telefone</label>
                        <input class="form-control" name="phone_number" placeholder="(00) 00000-0000)" required>
                    </div>
                </div>

                <div class="col-sm-2">
                    <button 
                        type="submit" 
                        class="btn btn-block btn-primary"
                        style="margin-top: 1.8em;"
                    >
                        Adicionar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>