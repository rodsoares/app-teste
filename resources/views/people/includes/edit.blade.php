<!-- Modal -->
<div class="modal fade" id="modal-edit-person" tabindex="-1" role="dialog" aria-labelledby="modal-edit-personLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-edit-personLabel"></h4>
            </div>
            <form id="form-edit-person" method="POST">
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name">Nome</label>
                                    <input class="form-control" name="name" placeholder="Digite um nome">
                                </div>
                            </div>
                        

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="cpf">CPF</label>
                                    <input class="form-control" name="cpf" placeholder="000.000.000-00">
                                </div>
                            </div>
                            
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="phone_number">Telefone</label>
                                    <input class="form-control" name="phone_number" placeholder="(00) 00000-0000" required>
                                </div>
                            </div>

                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Salvar alterações</button>
                </div>
            </form>
        </div>
    </div>
</div>