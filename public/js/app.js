function editPerson( id ){
    var modal = $('#modal-edit-person');
    var button = $('#edit-button-'+id);
    button.button('loading');
    
    /** obtendo valores */
    var url = location.href + '/' + id;
    var form = document.querySelector('#modal-edit-person #form-edit-person')

    form.reset()

    $.get(url)
      .done(function(response){
        // populando campos
        console.log( response.data )
        $('#modal-edit-person #form-edit-person input[name="name"]').attr('value', response.data.name);
        $('#modal-edit-person #form-edit-person input[name="cpf"]').attr('value', response.data.cpf);
        $('#modal-edit-person #form-edit-person input[name="phone_number"]').attr('value', response.data.phone_number);

        // acertando url
        $('#modal-edit-person #form-edit-person').attr('action', url);

        // exibindo modal
        button.button('reset');
        modal.modal('show');
      })
      .fail(function(){
        button.button('reset');
        swal({
            title: "Erro na requisição ?",
            text: "Algo ocorreu e a requisição não pôde ser completada. Tente novamente em alguns instantes!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
      });
}
    
function deletePerson( id ){
    swal({
        title: "Você tem certeza ?",
        text: "Uma vez excluído você não será capaz de recuperá-lo!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            var form = $('#form-delete-person');
            var url = location.href + '/' + id;
            console.log(form)
            console.log(url)
            deletePersonData( form, url );
        }
    });
}

function deletePersonData( form, url )
{
    $.post(url, form.serialize())
        .done(function (response) {
            swal("Ok! Pessoa deletada com sucesso da base de dados", {
                icon: "success",
            }).then(function(){
                location.reload();
            });
        })
        .fail(function (response) {
            swal({
                title: "Problema!",
                text: "Ocorreu algum erro. Tente novamente.",
                icon: "error",
            });
        });
}