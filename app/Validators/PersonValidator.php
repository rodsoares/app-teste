<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PersonValidator.
 *
 * @package namespace App\Validators;
 */
class PersonValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|string',
            'cpf'  => 'required|string|unique:people,cpf',
            'phone_number' => 'required|string|unique:people,phone_number'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'filled|string',
            'cpf'  => 'filled|string',
            'phone_number' => 'filled|string'
        ],
    ];
}
