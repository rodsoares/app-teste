<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PersonCreateRequest;
use App\Http\Requests\PersonUpdateRequest;
use App\Repositories\PersonRepository;
use App\Validators\PersonValidator;


class PeopleController extends Controller
{
    
    protected $repository;

    protected $validator;

    public function __construct(PersonRepository $repository, PersonValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $people = $this->repository->orderBy('id', 'desc')->get();

        if (request()->wantsJson()) {

            foreach( $people as $person )
            {
                $person->actions = '<div class="text-right">
                <button type="button" class="btn btn-xs btn-warning" onclick="editPerson('.$person->id.')">
                <i class="fa fa-fw fa-edit"></i> editar
                </button>
                <button type="button" class="btn btn-xs btn-danger" onclick="deletePerson('.$person->id.')">
                <i class="fa fa-fw fa-trash"></i> deletar
                </button>
                </div>';    
            }

            return response()->json([
                'data' => $people,
            ]);
        }

        return view('people.index', compact('people'));
    }

    public function store(PersonCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $person = $this->repository->create($request->all());

            $response = [
                'message' => 'Person created.',
                'data'    => $person->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    public function show($id)
    {
        $person = $this->repository->find($id);

        return response()->json([
            'data' => $person,
        ]);
    }

    public function edit($id)
    {
        $person = $this->repository->find($id);

        return view('people.edit', compact('person'));
    }

    public function update(PersonUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $person = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Person updated.',
                'data'    => $person->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Person deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Person deleted.');
    }
}
