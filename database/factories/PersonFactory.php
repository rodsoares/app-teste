<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Person::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('pt_BR');
    return [
        'name' => $faker->unique()->name,
        'cpf' => $faker->unique()->cpf,
        'phone_number' => $faker->unique()->cellphoneNumber
    ];
});
